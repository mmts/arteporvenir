<!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7" <?php language_attributes(); ?>>
<![endif]-->

<!--[if IE 8]>
<html class="ie ie8" <?php language_attributes(); ?>>
<![endif]-->

<!--[if !(IE 7) | !(IE 8)  ]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, maximum-scale=1.0, minimum-scale=1.0, initial-scale=1.0">
    <title><?php wp_title('-', true, 'right'); ?></title>
    <meta name="description" content="<?php bloginfo( 'description' ); ?>">
    <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">



  <!-- FIX HTML STYLES IE9 -->
    <!--[if gte IE 9]>
        <style type="text/css">
            .gradient {
                filter: none;
            }
        </style>
   <![endif]-->

   <!-- FIX HTML JS-->
    <!--[if lt IE 9]>
    <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://use.fontawesome.com/3786aff0c3.js"></script>

    <?php wp_enqueue_script("jquery"); ?>
    <?php wp_head();
    of_get_option('logo_uploader','Default Data');
    ?>
</head>
<body class="bg-gray">
<header>
  <?php $lang=pll_current_language(); ?>
  <nav class="navbar navbar-default col-xs-12 fm-header">
    <div class="container">
      <div class="row">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" id="nv-ho">
            <img src="<?php echo get_template_directory_uri(); ?>/images/logo.png">
          </a>
        </div>

<!--          --><?php
//            if ($lang == "en") {
//              echo '<div class="fm-trns-btn fm-trns-btn-1 visible-xs hidden-sm hidden-md hidden-lg active"><a class="no-padding" href="' . get_site_url() . '/en/">Eng</a></div>';
//              echo '<div class="fm-trns-btn fm-trns-btn-2 visible-xs hidden-sm hidden-md hidden-lg"><a class="no-padding" href="' . get_site_url() . '/es/">Esp</a></div>';
//            } else {
//              echo '<div class="fm-trns-btn fm-trns-btn-1 visible-xs hidden-sm hidden-md hidden-lg"><a class="no-padding" href="' . get_site_url() . '/en/">Eng</a></div>';
//              echo '<div class="fm-trns-btn fm-trns-btn-2 visible-xs hidden-sm hidden-md hidden-lg active"><a class="no-padding" href="' . get_site_url() . '/es/">Esp</a></div>';
//            }
//          ?>
        <!-- Collect the nav links, forms, and other content for toggling -->
<!--        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">-->
<!--          <ul class="nav navbar-nav navbar-right mt-5">-->
<!--            --><?php
//              if ($lang == "en") {
//                echo '<li class="fm-trns-btn hidden-xs visible-sm visible-md visible-lg pull-right"><a class="no-padding" href="' . get_site_url() . '/es/">Esp</a></li>';
//                echo '<li class="fm-trns-btn hidden-xs visible-sm visible-md visible-lg active pull-right"><a class="no-padding" href="' . get_site_url() . '/en/">Eng</a></li>';
//                echo '<li class="no-padding text-center pull-right fm-li-mob"><a class="nav-li" id="nv-co">Contact</a></li>';
//                echo '<li class="no-padding text-center pull-right fm-li-mob"><a class="nav-li" id="nv-st">Stories</a></li>';
//                echo '<li class="no-padding text-center pull-right fm-li-mob"><a class="nav-li" id="nv-wo">Work</a></li>';
//                echo '<li class="no-padding text-center pull-right fm-li-mob"><a class="nav-li" id="nv-te">Services</a></li>';
//              } else {
//                echo '<li class="fm-trns-btn hidden-xs visible-sm visible-md visible-lg active pull-right"><a class="no-padding" href="' . get_site_url() . '/es/">Esp</a></li>';
//                echo '<li class="fm-trns-btn hidden-xs visible-sm visible-md visible-lg pull-right"><a class="no-padding" href="' . get_site_url() . '/en/">Eng</a></li>';
//                echo '<li class="no-padding text-center pull-right fm-li-mob"><a class="nav-li" id="nv-co">Contacto</a></li>';
//                echo '<li class="no-padding text-center pull-right fm-li-mob"><a class="nav-li" id="nv-st">Historias</a></li>';
//                echo '<li class="no-padding text-center pull-right fm-li-mob"><a class="nav-li" id="nv-wo">Trabajo</a></li>';
//                echo '<li class="no-padding text-center pull-right fm-li-mob"><a class="nav-li" id="nv-te">Servicios</a></li>';
//              }
//            ?>


          </ul>
        </div><!-- /.navbar-collapse -->
      </div>
    </div><!-- /.container-fluid -->
  </nav>

 </header>