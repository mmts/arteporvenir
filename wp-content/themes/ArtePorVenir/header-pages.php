<!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7" <?php language_attributes(); ?>>
<![endif]-->

<!--[if IE 8]>
<html class="ie ie8" <?php language_attributes(); ?>>
<![endif]-->

<!--[if !(IE 7) | !(IE 8)  ]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, maximum-scale=1.0, minimum-scale=1.0, initial-scale=1.0">
    <title><?php wp_title('-', true, 'right'); ?></title>
    <meta name="description" content="<?php bloginfo( 'description' ); ?>">
    <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">



  <!-- FIX HTML STYLES IE9 -->
    <!--[if gte IE 9]>
        <style type="text/css">
            .gradient {
                filter: none;
            }
        </style>
   <![endif]-->

   <!-- FIX HTML JS-->
    <!--[if lt IE 9]>
    <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
    <?php wp_enqueue_script("jquery"); ?>
    <?php wp_head();
    of_get_option('logo_uploader','Default Data'); ?>
</head>
<body>
<div id="wptime-plugin-preloader"></div>
<header>
    <nav class="navbar navbar-default navbar-fixed-top active " role="navigation" id="mainMenu">
      <div class="cont-header container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
          
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-ex6-collapse">
            <span class="sr-only">Desplegar navegación</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand img-responsive" href="<?php bloginfo('url')?>">
            <img class="brand-white" src="<?php bloginfo('template_url'); ?>/images/logoTeraWhite.png"/>
            <img class="brand-color hide" src="<?php bloginfo('template_url'); ?>/images/logoTera.png"/>
          </a>
          <div class="cont-icon hidden-md hidden-lg">
            <!-- <i class="icon icon-chat regular pull-right"></i> -->
            <i class="icon icon-phone regular pull-right"></i>
          </div>
        </div> 
        
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div id="navbar-ex6-collapse" class="collapse navbar-collapse navbar-ex6-collapse">
          <?php
              wp_nav_menu( array(
                'menu'              => 'extra-menu',
                'theme_location'    => 'extra-menu',
                'depth'             => 2,
                'container'         => '',
                //'container'         => 'div',
                //'container_class'   => 'collapse navbar-collapse',
                //'container_id'      => 'bs-example-navbar-collapse-1',
                'menu_class'        => 'nav navbar-nav navbar-right',
                'fallback_cb'       => 'wp_bootstrap_navwalker::fallback',
                'walker'            => new wp_bootstrap_navwalker())
              );
          ?>
          <div class="center visible-xs visible-sm">
            <button type="button" class="btn btn-default btn-a">try us for <b>free!</b></button>
        </div>
        </div><!-- /.navbar-collapse -->
        
      </div><!-- /.container -->
    </nav>