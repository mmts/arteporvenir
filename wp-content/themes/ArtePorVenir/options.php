<?php

function optionsframework_option_name() {
	// This gets the theme name from the stylesheet
	$themename = wp_get_theme();
	$themename = preg_replace("/W/", "_", strtolower($themename) );

	$optionsframework_settings = get_option( 'optionsframework' );
	$optionsframework_settings['id'] = $themename;
	update_option( 'optionsframework', $optionsframework_settings );
}

/**
 * Defines an array of options that will be used to generate the settings page and be saved in the database.
 * When creating the 'id' fields, make sure to use all lowercase and no spaces.
 *
 * If you are making your theme translatable, you should replace 'theme-textdomain'
 * with the actual text domain for your theme.  Read more:
 * http://codex.wordpress.org/Function_Reference/load_theme_textdomain
 */

function optionsframework_options() {

/************** AQUÍ TODAS NUESTRAS OPCIONES *********************/

//Pestaña Configuración general
$options[] = array(
     'name' => __('General settings', 'options_framework_theme'),
     'type' => 'heading');
//Agregando logo brand
$options[] = array(
     'name' => __('Logo Header 1', 'options_check'),
     'desc' => __('Select the logo to be displayed on the web, tamaño máximo 171 x 114px.', 'options_check'),
     'id' => 'logo_white',
     'type' => 'upload');

$options[] = array(
     'name' => __('Logo Header 2', 'options_check'),
     'desc' => __('Select the logo to be displayed on the web, tamaño máximo 292 px 114px.', 'options_check'),
     'id' => 'logo_color',
     'type' => 'upload');

$options[] = array(
     'name' => __('Video Home', 'options_check'),
     'desc' => __('Select the video to be displayed on the web, tamaño máximo 1280px x 720px.', 'options_check'),
     'id' => 'video_home',
     'type' => 'upload');


//Pestaña información de contacto
$options[] = array(
     'name' => __('Contact Info', 'options_framework_theme'),
     'type' => 'heading' );


	return $options;

}

