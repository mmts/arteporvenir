<?php
//Loads css file
wp_enqueue_style( 'bootstrap', get_template_directory_uri() . '/css/bootstrap.min.css', array(), '3.3.6' );	
wp_enqueue_style( 'bootstrap-theme', get_template_directory_uri() . '/css/bootstrap-theme.min.css', array(), '3.3.6' );
wp_enqueue_style( 'slick', get_template_directory_uri() . '/css/slick.css', array(), '1' );
wp_enqueue_style( 'slick-theme', get_template_directory_uri() . '/css/slick-theme.css', array(), '1' );

wp_enqueue_style( 'style', get_template_directory_uri() . '/css/style.css', array(), '1' );
wp_enqueue_style( 'arrows', get_template_directory_uri() . '/css/arrows.css', array(), '1' );

//Loads JavaScript file
wp_enqueue_script( 'bootstrap-min-js', get_template_directory_uri() . '/js/bootstrap.min.js', array( 'jquery' ), '2016-01-18', false );
wp_enqueue_script( 'index', get_template_directory_uri() . '/js/index.js', array( 'jquery' ), '2015-01-18', false );

/* Cargar Panel de Opciones
/*-----------------------------------------*/
if ( !function_exists( 'optionsframework_init' ) ) {
define( 'OPTIONS_FRAMEWORK_DIRECTORY', get_template_directory_uri() . '/inc/' );
require_once dirname( __FILE__ ) . '/inc/options-framework.php';
}


function link_func( $atts ) {
    $a = shortcode_atts( array(
        'permalink' => '/',
    ), $atts );
    return  'href="'.site_url($a['permalink']).'"';
}
add_shortcode( 'link', 'link_func' );


add_action('get_header', 'remove_admin_login_header');
function remove_admin_login_header() {
    remove_action('wp_head', '_admin_bar_bump_cb');
}

// Add support SVG files
function cc_mime_types($mimes) {
  $mimes['svg'] = 'image/svg+xml';
  return $mimes;
}
add_filter('upload_mimes', 'cc_mime_types');

/*************************************************************
 ***************   Start Custom Type ArtePost   ***************
 **************************************************************/
function PostTypeContact() {
  $labels = array(
    'type'                  => 'artepost',
    'name'                  => 'ArtePost',
    'singular_name'         => 'ArtePost',
    'menu_name'             => 'ArtePost',
    'name_admin_bar'        => 'ArtePost',
    'archives'              => 'Item Archives',
    'parent_item_colon'     => 'Parent Item:',
    'all_items'             => 'All ArtePost',
    'add_new_item'          => 'Add New ArtePost',
    'add_new'               => 'Add New ArtePost',
    'new_item'              => 'New Item',
    'edit_item'             => 'Edit ArtePost',
    'update_item'           => 'Update ArtePost',
    'view_item'             => 'View ArtePost',
    'search_items'          => 'Search ArtePost',
    'not_found'             => 'Not found',
    'not_found_in_trash'    => 'Not found in Trash',
    'featured_image'        => 'Featured Image',
    'set_featured_image'    => 'Set featured image',
    'remove_featured_image' => 'Remove featured image',
    'use_featured_image'    => 'Use as featured image',
    'insert_into_item'      => 'Insert into item',
    'uploaded_to_this_item' => 'Uploaded to this item',
    'items_list'            => 'Items list',
    'items_list_navigation' => 'Items list navigation',
    'filter_items_list'     => 'Filter items list',
  );
  $args = array(
    'label'                 => 'ArtePost',
    'description'           => 'Add ArtePost',
    'labels'                => $labels,
    'supports'              => array( 'title','author','page-attributes',),
    'taxonomies'            => array( 'category', ),
    'hierarchical'          => false,
    'public'                => true,
    'show_ui'               => true,
    'show_in_menu'          => true,
    'menu_position'         => 25,
    'menu_icon'             => 'dashicons-sos',
    'show_in_admin_bar'     => true,
    'show_in_nav_menus'     => true,
    'can_export'            => true,
    'has_archive'           => true,
    'exclude_from_search'   => false,
    'publicly_queryable'    => true,
    'capability_type'       => 'post',
  );
  register_post_type( 'post_contact', $args );

}
add_action( 'init', 'PostTypeContact', 0 );


add_filter( 'rwmb_meta_boxes', 'MetaBoxesContact' );
function MetaBoxesContact( $meta_boxes )
{
  $prefix = 'contact';
  $meta_boxes[] = array(
    'title'  => __( 'Header Content' ),
    'post_types' => 'post_contact',
    'fields' => array(
      array(
        'id'          => 'artepost_title',
        'desc'        => __( 'Title', 'your-prefix' ),
        'type'        => 'text',
        'clone'       => false,
        'placeholder' => __( 'Enter the title here', 'your-prefix' ),
        'size'        => 50,
      ),
      array(
        'id'          => 'artepost-message',
        'desc'        => __( 'Post Message', 'your-prefix' ),
        'type'        => 'textarea',
        'clone'       => false,
        'placeholder' => __( 'Enter the message here', 'your-prefix' ),
        'rows'        => 5,
        'cols'        => 5,
      ),
      array(
        'id'          => 'artepost-video',
        'desc'        => __( 'Video URL', 'your-prefix' ),
        'type'        => 'text',
        'clone'       => false,
        'placeholder' => __( 'Enter the video URL here', 'your-prefix' ),
        'size'        => 50,
      ),
      array(
        'id'   => 'artepost-photo',
        'name' => __( 'Post Photo' ),
        'desc'  => 'Please, upload your photo',
        'type' => 'image_advanced',
        'max_file_uploads' => 1
      ),
    ),
  );
  return $meta_boxes;
}
/**************************************************************
 ***************    End Custom Type ArtePost     ***************
 **************************************************************/